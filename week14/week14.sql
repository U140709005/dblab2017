#initial time=6.672
#after index=2.359
#after PK=2.797

use uniprot_example;
select * from protein;
#delete from protein;
load data local infile 'C:\\insert.txt' into table protein fields terminated by "|";

select *
from protein
where protein_name like "%tumor%" and uniprot_id like "%human%"
order by uniprot_id;

create index uniprot_id on protein(uniprot_id);
drop index uniprot_id on protein;



alter table protein add constraint pk_protein primary key(uniprot_id);
alter table protein drop primary key