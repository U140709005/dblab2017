use movie_db;

alter table movies drop primary key;
alter table countries drop primary key;
alter table directors drop primary key;
alter table genres drop primary key;
alter table languages drop primary key;
alter table movie_directors drop primary key;
alter table movie_stars drop primary key;
alter table producer_countries drop primary key;
alter table stars drop primary key;


load data local infile 'C:\\\Users\\Ali\\Desktop\\movies.csv' into table movies fields terminated by';';
load data local infile 'C:\\Users\\Ali\\Desktop\\directors.csv' into table directors fields terminated by';';
load data local infile 'C:\\Users\\Ali\\Desktop\\stars.csv' into table stars fields terminated by';';
load data local infile 'C:\\Users\\Ali\\Desktop\\countries.csv' into table countries fields terminated by';';
load data local infile 'C:\\Users\\Ali\\Desktop\\languages.csv' into table languages fields terminated by';';
load data local infile 'C:\\Users\\Ali\\Desktop\\genres.csv' into table genres fields terminated by';';
load data local infile 'C:\\Users\\Ali\\Desktop\\movie_directors.csv' into table movie_directors fields terminated by';';
load data local infile 'C:\\Users\\Ali\\Desktop\\movies_stars.csv' into table movie_stars fields terminated by';';
load data local infile 'C:\\Users\\Ali\\Desktop\\producer_countries.csv' into table producer_countries fields terminated by';';


alter table movies add primary key (movie_id);
alter table countries add primary key (country_id);
alter table directors add primary key (director_id);
alter table genres add primary key (movie_id,genre_name);
alter table languages add primary key (movie_id,language_name);
alter table movie_directors add primary key (movie_id,director_id);
alter table movie_stars add primary key (movie_id,star_id);
alter table producer_countries add primary key (movie_id,_country_id);
alter table stars add primary key (star_id);